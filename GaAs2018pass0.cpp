#include <iostream>
#include <unistd.h>	//getopt()
#include <sstream>	//stringstream
#include <map>

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "GaAs2018.h"

using namespace std;

int trig_time_file(uint32_t);
string my_get_start(FILE*);
string my_get_string(FILE*);

int main(int argc, char** argv)
{
  const string revision(MYREVISION);

// Check command line arguments and get the run number to process
  uint32_t runnumber(0);
  string execname(argv[0]);
  size_t found = execname.find_last_of("/");
  if(found != string::npos)
    execname = execname.substr(found+1);
  if(argc < 2) {
    cout << execname << ": use -h option for help" << endl;
    exit(0);
  }
  bool calib(false);
  int32_t c;
  while ((c=getopt(argc, argv, "chr:v")) != -1) {
    switch(c) {
      case 'c':
        calib = true;
        break;
      case 'h':
        cout << execname << " options:" << endl;
        cout << "  -c    - calibration mode (no trigger file is supplied)" << endl;
        cout << "  -h    - print this help message and exit" << endl;
        cout << "  -r NN - process run number NN" << endl;
        cout << "  -v    - print version number and exit" << endl;
        exit(0);
      case 'r':
        runnumber = atoi(optarg);
        if(runnumber < runnumb_min || runnumber > runnumb_max) {
          cout << "Error! Illegal run number: " << runnumber << endl;
          exit(1);
        }
        break;
      case 'v':
        cout << execname << " " << revision << endl;
        exit(0);
      default:
        cout << execname << ": use -h option for help" << endl;
        exit(1);
    }
  }

// Output ROOT file, histograms and trees
  stringstream ssroot;
  ssroot << "run_" << setfill('0') << setw(4) << runnumber << ".root";
  TFile* froot = new TFile(ssroot.str().c_str(),"RECREATE");
  if(froot==NULL)
    { cout << "ERROR: could not open Root file" << endl; exit(-1); }
  froot->cd();

// Process trigger timing file or skip this part for calibration mode
  if(!calib) {
    if(trig_time_file(runnumber)) {
      cout << "Error from trig_time_file(" << runnumber << ")\n";
      froot->Close();
      exit(-1);
    }
  }

  TH1F* hTpix  = new TH1F("hTpix","Pixels timing (sec)", 20000,0,2000);
  TH1F* hToT   = new TH1F("hToT","Time-over-Threshold", 400,0,400);
  TH1F* hX     = new TH1F("hX","Pixel X coord", 256, 0,256);
  TH1F* hY     = new TH1F("hY","Pixel Y coord", 256, 0,256);

// Process pixels data file
  GAASpixel_t pixel;
  TTree* gaas_data = new TTree("gaas_data", "GaAs pixel detector: pixel data, June 2018");
  gaas_data->Branch("pixels",&pixel.time1ns,"time1ns/l:X/s:Y/s:ToT/s");
  stringstream ssdata;
  ssdata << "run_" << setfill('0') << setw(4) << runnumber << ".txt";
  string filedata = filepath + "RawData/" + ssdata.str();
  cout << "Data file:    " << filedata << endl;
  FILE* fd = fopen(filedata.c_str(), "r");
  if (fd == NULL) {
    cout << "Error! unable to open data file" << endl;
    exit(-1);
  }
  int32_t XY;
  int64_t time25ns;
  int32_t time1_56ns;
  int npix(0);
  string str0 = my_get_start(fd);
//  cout << "Control sequence: " << str0 << endl;
  string mystr;
  while (1) {
    mystr = my_get_string(fd);
    if(mystr.find("ERROR") != string::npos || mystr.find("INFO") != string::npos) break;
    if(mystr[0] == '#') continue;
    npix++;
    istringstream iss(mystr);
    iss >> XY >> time25ns >> time1_56ns >> pixel.ToT;
    pixel.time1ns = time25ns * 25 + time1_56ns * 1.5625 + 0.5;
    pixel.X = XY%256;
    pixel.Y = XY/256;
    hTpix->Fill(pixel.time1ns * 1e-9);
    hToT->Fill(pixel.ToT);
    hX->Fill(pixel.X);
    hY->Fill(pixel.Y);
    gaas_data->Fill();
  }
  fclose(fd);
  cout << mystr << endl;
  cout << "number of pixels: " << npix << endl;

  froot->Write();
  froot->Close();
  exit(0);
}
//--------------------------------------------------------------------
int trig_time_file(uint32_t runnumber) {
// Trigger timing file
  stringstream sstrig;
  sstrig << "run_" << setfill('0') << setw(4) << runnumber << "_trigger.txt";
  string filetrig = filepath + "RawData/" + sstrig.str();
  cout << "Trigger file: " << filetrig << endl;
  FILE* ft = fopen(filetrig.c_str(), "r");
  if (ft == NULL) {
    cout << "Error! Unable to open trigger file. " << endl;
//    froot->Close();
    return -1;
  }
  string str0 = my_get_start(ft);
//  cout << "Control sequence: " << str0 << endl;

  TH1F* hTtrig = new TH1F("hTtrig","Trigger arrival time (sec)", 20000,0,2000);
  TH1F *hTrigDt= new TH1F("hTrigDt","Interval between triggers (sec)", 2000,0,0.05);
  TH1F* hTstart= new TH1F("hTstart","Spill start time (sec)",200000,0,2000);
  TH1F* hTend  = new TH1F("hTend","Spill end time (sec)",200000,0,2000);
  TH1F* hSpLen = new TH1F("hSpLen","SPS spill length (sec)", 1000,0,20);
  TH1F* hSpInt = new TH1F("hSpInt","Interval between SPS spills (sec)", 2000,0,50);
  TH1F* hNtrig = new TH1F("hNtrig","Number of triggers in SPS spill", 125,0,2500);

  int ntrig(0);
  map<uint64_t, uint16_t> trig_map;
  trig_map.clear();
  double time;
  double prev(0);
  uint16_t flag;
  uint64_t t_trig;
  string mystr;
  while (1) {
    mystr = my_get_string(ft);
    //cout << mystr << endl;
    if(mystr.find("ERROR") != string::npos || mystr.find("INFO") != string::npos) break;
    if(mystr[0] == '#') continue;
    ntrig++;
    istringstream iss(mystr);
    iss >> flag >> time;
    hTtrig->Fill(time);
    t_trig = time * 1e9 + 0.5;
    trig_map[t_trig] = flag;
    hTrigDt->Fill(time - prev);
    prev = time;
  }
  fclose(ft);
  cout << mystr << endl;
  cout << "number of triggers: " << dec << ntrig << endl;

//Reconstruct SPS spill structure
  struct Spill_t{
    uint64_t start;
    uint64_t end;
  };
  int ntr_prev(0);
  const int nthr1(7);
  const int nthr2(5);
  int ntr1s;
  unsigned int nspill(0);
  Spill_t spill;
  uint64_t t_next;
  uint64_t sp_prev(0);
  spill.start=0;
  spill.end  =0;
  bool inside_spill(false);
  vector<Spill_t> spills;
  spills.clear();
  map<uint64_t,uint16_t>::iterator i,j,k;
  for(i=trig_map.begin(); i!=trig_map.end(); ++i) {
    t_trig = i->first;
    ntr1s=0;
    k = i;
    k++;
    for(j=k; j!=trig_map.end(); ++j) {
      t_next = j->first; 
      if(t_next-t_trig > 1e8) break;  //number of subsequent triggers within next 0.1 sec
      ntr1s++;
    }
// Artificial cut 1: time between spills should be greater than 12 sec
    if(nspill < 2 || (t_trig-spill.start > 12e9)) {
      if(!inside_spill && ntr_prev < nthr1 && ntr1s >= nthr1) {
        if(nspill) {
          hTend->Fill(spill.end*1e-9,20);
          hSpLen->Fill((spill.end-spill.start)*1e-9);
          hSpInt->Fill((spill.start-sp_prev)*1e-9);
          cout << "  " << spill.end*1e-9 << "  " << (spill.end-spill.start)*1e-9;
          if(nspill>1)
            cout << "  " << (spill.start-sp_prev)*1e-9;
          cout << endl;
          spills.push_back(spill);
        }
        sp_prev = spill.start;
        spill.start = t_trig;
        hTstart->Fill(spill.start*1e-9,20);
        nspill++;
        inside_spill = true;
        cout << nspill << "   " << spill.start*1e-9;
      }
    }
// Artificial cut 2: spill length should be less than 6 sec
    if(nspill > 0 && (t_trig-spill.start < 6e9)) {
      if(ntr_prev >= nthr2 && ntr1s < nthr2) {
        spill.end = t_trig;
        inside_spill = false;
  //    cout << "     end=" << t_trig*1e-9 << endl;
      } 
    }
    ntr_prev = ntr1s;
  }
// The last end-of-spill
  if(inside_spill)
    spill.end = t_trig;
  spills.push_back(spill);
  hTend->Fill(spill.end*1e-9,20);
  hSpLen->Fill((spill.end-spill.start)*1e-9);
  hSpInt->Fill((spill.start-sp_prev)*1e-9);
  cout << "  " << spill.end*1e-9 << "  " << (spill.end-spill.start)*1e-9 << "  " << (spill.start-sp_prev)*1e-9 << endl;
// Filling the trigger tree from trig_map
  GAAStrigger_t trigger;
  TTree* gaas_trig = new TTree("gaas_trig", "GaAs pixel detector: triggers, June 2018");
  gaas_trig->Branch("triggers",&trigger.time1ns,"time1ns/l:flag/s:spill/s");
  int m;
  int mp(0);
  int ntrspill(0);
  for(auto i: trig_map) {
    trigger.time1ns = i.first;
    trigger.flag = i.second;
    trigger.spill = 0;
    m=0;
    for(auto j: spills) {
      m++;
      if(j.start <= trigger.time1ns && trigger.time1ns <= j.end) {
        trigger.spill = m;
        break;
      }
    }
    if(trigger.spill!=mp) {
      if(ntrspill) hNtrig->Fill(ntrspill);
      ntrspill=0;
    }
    if(trigger.spill) ntrspill++;
    mp=trigger.spill;
    gaas_trig->Fill();
  }
  cout << "number of spills: " << nspill << endl;

  return 0;
}
//--------------------------------------------------------------------
string my_get_start(FILE* myfile) {
  uint8_t  b8;
  uint16_t b16[3];
  for(int i=0; i<3; i++) {
    if(fread( &b8, sizeof(uint8_t),1,myfile) != 1) {
      string str = "ERROR: file read error in function my_get_start()";
      return str;
    }
    b16[i] = b8;
  }
  ostringstream oss;
  oss << hex << b16[0] << b16[1] << b16[2];
  return oss.str();
}
//--------------------------------------------------------------------
string my_get_string(FILE* myfile) {
  uint8_t b8;
  bool OdOa(false);
  string str;
  str.clear();

  while (!feof(myfile)) {
    if (fread(&b8, sizeof(uint8_t),1,myfile) != 1) {
      str = "ERROR: file read error";
      if (feof(myfile)) break;
      return str;
    }
    if(OdOa) {
      if(b8 == 0x0a) return str;
      str = "ERROR: end-of-line 0d0a sequence error";
      return str;
    }
    if(b8 == 0x0d) {
      OdOa = true;
      continue;
    }
    str.push_back((char)b8);
  }
  str = "INFO: end-of-file";
  return str;
}
//--------------------------------------------------------------------
