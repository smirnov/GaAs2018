#include <iostream>
#include <sstream>
#include <unistd.h>

#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TH2.h"

#include "GaAs2018.h"
#include "PixCalib.h"

using namespace std;

void print_help(string en) {
  cout << en << " options:" << endl;
  cout << "  -c    - calibration mode (i.e. without triggers)" << endl;
  cout << "  -h    - print this help message and exit" << endl;
  cout << "  -n N  - limit processing to N events" << endl;
  cout << "  -r NN - process run number NN" << endl;
  cout << "  -v    - print version number and exit" << endl;
}

int main(int argc, char** argv)
{
  const string revision(MYREVISION);

// Check command line arguments and get the run number for processing
  uint32_t nstop(0);
  uint32_t runnumber(0);
  string execname(argv[0]);
  size_t found = execname.find_last_of("/");
  if(found != string::npos)
    execname = execname.substr(found+1);
  if(argc < 2) {
    print_help(execname);
    exit(0);
  }
  int32_t c;
  bool calib(false);
  while ((c=getopt(argc, argv, "chn:r:v")) != -1) {
    switch(c) {
      case 'c':
        calib = true;
        break;
      case 'h':
        print_help(execname);
        return 0;
      case 'n':
        nstop = atoi(optarg);
        break;
      case 'r':
        runnumber = atoi(optarg);
        if(runnumber < runnumb_min || runnumber > runnumb_max) {
          cout << "Error! Illegal run number: " << runnumber << endl;
          exit(-1);
        }
        break;
      case 'v':
        cout << execname << " " << revision << endl;
        exit(0);
      default:
        cout << execname << ": use -h option for help" << endl;
        exit(1);
    }
  }

  stringstream ssroot;
  ssroot << filepath << "Analysis/pass0/run_" << setfill('0') << setw(4) << runnumber << ".root";
  TFile* f0=new TFile(ssroot.str().c_str(),"READ");
  if(f0->IsZombie())
    { cout << "ERROR: could not open input Root file:" << endl; cout << ssroot.str() << endl; exit(-1); }

  GAAStrigger_t trigger;
  GAASpixel_t   pixel;

  Long64_t ntrig(0);
  TTree *gaas_trig=(TTree*)f0->Get("gaas_trig");
  if(!calib && gaas_trig) {
    gaas_trig->SetBranchAddress("triggers",&trigger.time1ns);
    ntrig = gaas_trig->GetEntries();
    cout << "Ntrig = " << ntrig << endl;
  }

  TTree *gaas_data=(TTree*)f0->Get("gaas_data");
  gaas_data->SetBranchAddress("pixels",&pixel.time1ns);

  vector<GAAStrigger_t> triggers;
  vector<GAASpixel_t>   pixels;
  vector<bool>      pix_assigned;
  Long64_t nhits = gaas_data->GetEntries();
  cout << "Nhits=" << nhits << endl;

  stringstream ssout;
  ssout << "run_" << setfill('0') << setw(4) << runnumber << "_1v2.root";
  TFile* f1 = new TFile(ssout.str().c_str(),"RECREATE");
  if(f1->IsZombie()) { cout << "ERROR: could not open output Root file" << endl; f0->Close(); exit(-1); }
  f1->cd();

  TH1F* hDiff0 = new TH1F("hDiff0","Trigger time interval (sec) out-of-spill",500,0,0.5);
  TH1F* hDiff1 = new TH1F("hDiff1","Trigger time interval (sec) inside spill",500,0,0.5);
  TH1F* hdtm = new TH1F("hdtm", "Min delta T trigger - pixel (ns)", 4000,-2000,2000);
  TH1F *hdt  = new TH1F("hdt","Delta T trigger - pixel (ns)", 4000,-2000,2000);
  TH1F* hNpix= new TH1F("hNpix","Nb of fired pixels in event",100, 0,100);
  TH1F* hTpix  = new TH1F("hTpix","Triggered pixels timing (sec)", 20000,0,2000);
  TH1F* hTnnpix= new TH1F("hTnnpix","Non-triggered pixels timing (sec)", 20000,0,2000);
  TH1F* hToT   = new TH1F("hToT","Time-over-Threshold of triggered pixels", 400,0,400);
  TH1F* hToTnn = new TH1F("hToTnn","Time-over-Threshold of non-triggered pixels",400,0,400);
  TH2F* hXYpix   = new TH2F("hXYpix","X and Y of triggered pixels",256,0,256,256,0,256);
  TH2F* hXYnnpix = new TH2F("hXYnnpix","X and Y of non-triggered pixels", 256,0,256,256,0,256);

// read all trigger data from "gaas_trig/triggers" tree/branch and fill "triggers" vector in order
// to speed up processing in memory
  double dtr;
  uint64_t trig_prev(0);
  triggers.clear();
  for(Long64_t i=0; i<ntrig; i++) {
    gaas_trig->GetEntry(i);
    if(trigger.flag == trig_flag_pion) trigger.flag=0;  //unification of 2017 and 2018 data formats
    triggers.push_back(trigger);
    if(i) {
      dtr = (trigger.time1ns - trig_prev)*1e-9;
      if(trigger.spill)
        hDiff1->Fill(dtr);
      else
        hDiff0->Fill(dtr);
    }
    trig_prev = trigger.time1ns;
  }

// read all pixel data from "gaas_data/pixels" tree/branch and fill "pixels" and "pix_assigned" vectors
  pixels.clear();
  pix_assigned.clear();
  for(Long64_t j=0;j<nhits;j++) {
    gaas_data->GetEntry(j);
    pixels.push_back(pixel);
    pix_assigned.push_back(false);
  }

  PixCalib_t         pixc;
  vector<PixCalib_t> evpix;

  TTree* event = new TTree("event", "GaAs detector test beam events, June 2018");
  event->Branch("trig",   &trigger.time1ns,"time1ns/l:flag/s:spill/s");
  event->Branch("vectpix",&evpix);

  uint32_t nout(0);
  uint32_t jpixmin;
  uint32_t jpixmax;
  uint32_t i,j,j0,j1;
  uint32_t mtrig;
  Long64_t dt;
  Long64_t dtmin;
  Long64_t time_win0(trig_pix_time_min);
  Long64_t time_win1(trig_pix_time_max);
  if(calib) {
    time_win0 = -400;
    time_win1 =  400;
  }
  mtrig = calib ? pixels.size() : triggers.size();
  for(i=0;i<mtrig;i++) {
    if(calib) {
      if(pix_assigned[i]) continue;
      pix_assigned[i]=true;
      trigger.time1ns = pixels[i].time1ns;
      trigger.flag = 0;
      trigger.spill = 0;
    }
    else {
      trigger = triggers[i];
    }
    evpix.clear();
    dtmin = 0x7FFFFFFFFFFFFFFF;
    j0 = 0;
    jpixmin = j0;
    j1 = pixels.size();
    jpixmax = j1;
    for(j=jpixmin;j<jpixmax;j++) {
      if(pix_assigned[j]) continue;
      dt = trigger.time1ns - pixels[j].time1ns;
      if(dt > 5e7) j0=j;
      if(TMath::Abs(dt) < TMath::Abs(dtmin))
        dtmin = dt;
      hdt->Fill(dt);
      if(time_win0 < dt && dt < time_win1) {
        pix_assigned[j] = true;
        pixc.time1ns    = pixels[j].time1ns;
        pixc.energy     = 0;
        pixc.resolution = 0;
        pixc.X          = pixels[j].X;
        pixc.Y          = pixels[j].Y;
        pixc.ToT        = pixels[j].ToT;
        pixc.device     = 0;
        evpix.push_back(pixc);
      }
//      cout << "i,j = " << i << "  " << j << "  dt= " << dt << 
      if(dt < -5e7) { j1=j; break;}
    }
    jpixmin = j0;
    jpixmax = j1;
    hdtm->Fill(dtmin);
    hNpix->Fill(evpix.size());
    if(evpix.size() > 0) {
      event->Fill();
      nout++;
    }
    if(nstop && (nout >= nstop)) break;
    if(i%1000 == 0)
    { cout << "trigger no. " << i << "  event size " << evpix.size() << " pixels";
//      if(!evpix.size()) cout << "  dtmin = " << dtmin << " ns";
//      cout << "  jpixmin = " << jpixmin << "  jpixmax = " << jpixmax;
      cout << endl;
    }
  }

  uint32_t nnpix=0;
  for(uint32_t j=0;j<pixels.size();j++) {
    pixel = pixels[j];
    if(pix_assigned[j]) {
      hTpix->Fill(pixel.time1ns*1e-9);
      hToT->Fill(pixel.ToT);
      hXYpix->Fill(pixel.X,pixel.Y);;
    } else {
      nnpix++;
      hTnnpix->Fill(pixel.time1ns*1e-9);
      hToTnn->Fill(pixel.ToT);
      hXYnnpix->Fill(pixel.X,pixel.Y);
    }
  }
  f1->Write();
  f1->Close();
  cout << "Number of written events: " << nout << endl;
  cout << "Number of non-assigned pixels: " << nnpix << endl;

  return 0;
}
