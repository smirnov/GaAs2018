struct PixCalib_t {
  ULong64_t time1ns;
  Double_t  energy;
  Double_t  resolution;
  UShort_t  X;
  UShort_t  Y;
  UShort_t  ToT;
  UShort_t  device;
};
