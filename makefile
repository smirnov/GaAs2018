#
platform = $(shell uname -s)
#CXX =
CXXFLAGS = -Wall
ifeq ($(platform),Darwin)
  suffix=macos
endif
ifeq ($(platform),Linux)
  suffix=linux
endif
HEADREVISION = v0.0.7
#
pass0:	GaAs2018pass0.cpp GaAs2018.h
	$(CXX) $(CXXFLAGS) $(shell root-config --cflags --libs) -DMYREVISION=\"$(HEADREVISION)\" -o $@_$(suffix) $<
#
dict.cc:	PixCalib.h LinkDef.h
	rootcint -f dict.cc -c $^
#
pass1:	GaAs2018pass1.cpp GaAs2018.h
	$(CXX) $(CXXFLAGS) $(shell root-config --cflags --libs) -DMYREVISION=\"$(HEADREVISION)\" -o $@_$(suffix) $<
#
#pass2:	GaAs2018pass2.cpp GaAs2018.h dict.cc
#	$(CXX) $(shell root-config --cflags --libs) -DMYREVISION=\"$(HEADREVISION)\" -o $@_$(suffix) GaAs2018pass2.cpp dict.cc
#
clean:
	rm -f pass0_$(suffix) pass1_$(suffix)
##
