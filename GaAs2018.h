//Root-tree branch "triggers" contents:
struct GAAStrigger_t {
  ULong64_t time1ns;
  UShort_t  flag;
  UShort_t  spill;
};

//Root-tree branch "pixels" contents:
struct GAASpixel_t {
  ULong64_t time1ns;
  UShort_t  X;
  UShort_t  Y;
  UShort_t  ToT;
};

//Root-tree with pixel data prepared for calibration
struct pixcalib_t {
  ULong64_t time1ns;
  Double_t  en;
  Double_t  reso;
  UShort_t  X;
  UShort_t  Y;
  UShort_t  ToT;
};

//Parameters specific for 2018 test beam
const uint32_t runnumb_min(1);  //window for 2018 run numbers
const uint32_t runnumb_max(70);
const std::string filepath("/eos/atlas/atlascerngroupdisk/det-trt-tb/testbeam2018/GaAs/");
const Long64_t trig_pix_time_min(100);  //time window (ns) for trigger to pixel time interval
const Long64_t trig_pix_time_max(800);
const uint16_t trig_flag_pion(4);  //original (non-unified) raw data format:
const uint16_t trig_flag_elec(2);  //trigger flags for e/pi
